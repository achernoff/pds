package data;

import gui.OrderFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.Timer;

import domain.DeliveryStation;
import domain.Item;
import domain.KitchenStation;
import domain.Order;
import domain.OrderStatus;

/**
 * The OrderTracker class tracks Orders as they progress through the preparation
 * process. Additionally, it provides an external interface through which the
 * rest of the pizza delivery system accesses order information.
 * 
 * @author acc3863
 * 
 */
public class OrderTracker implements DataTracker<Order> {
	private static int nextOrderID = 0;
	private static String ORDER_FILE = "orders.dat";

	private List<Order> orders;
	private KitchenStation kitchen;
	private DeliveryStation delivery;
	private Timer timeTicker;
	private boolean dayRunning;
	private boolean isReadyToStart;
	private int chefs;
	private int ovens;
	private int ovenSpace;
	private int cars;

	/**
	 * Instantiates an OrderTracker object with no orders tracked.
	 * 
	 * @param chefs
	 *            - Number of preparation chefs.
	 * @param ovens
	 *            - Number of ovens.
	 * @param ovenspace
	 *            - Cooking space per oven.
	 * @param cars
	 *            - Number of delivery cars.
	 */
	public OrderTracker() {
		orders = new ArrayList<Order>();
		dayRunning = false;
		isReadyToStart = false;
		timeTicker = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				kitchen.tick();
				updateOrders();
				delivery.tick();
				OrderFrame.getInstance().updateOrders(orders);
			}

		});
		timeTicker.setDelay(1000);
		timeTicker.setInitialDelay(0);
		timeTicker.setRepeats(true);
	}
	
	public void initStations(int chefs, int ovens, int ovenspace, int cars) {
		this.chefs = chefs;
		this.ovens = ovens;
		this.ovenSpace = ovenspace;
		this.cars = cars;
		isReadyToStart = true;
	}

	/**
	 * Starts the Timer member, emitting an event every 1000ms to simulate time
	 * passing and the day starting.
	 */
	public void startDay() {
		if (isReadyToStart) {
			kitchen = new KitchenStation(chefs, ovens, ovenSpace);
			delivery = new DeliveryStation(cars);
			timeTicker.start();
			dayRunning = true;
		}
	}

	/**
	 * Returns true if the pizza store is running.
	 * 
	 * @return true if the pizza store is running.
	 */
	public boolean isDayRunning() {
		return dayRunning;
	}

	/**
	 * Ends the day at the pizza store, stopping the Timer.
	 */
	public void endDay() {
		timeTicker.stop();
		dayRunning = false;
	}

	/**
	 * Updates the states of all orders tracked by this OrderTracker.
	 */
	public void updateOrders() {
		Iterator<Order> iter = orders.listIterator();
		while (iter.hasNext()) {
			Order o = iter.next();
			if (o.getStatus() == OrderStatus.IN_PREP) {
				boolean allDone = true;
				for (Item i : o.getItems()) {
					if (!i.isPrepared()) {
						allDone = false;
						break;
					}
				}
				if (allDone) {
					o.setStatus(OrderStatus.AWAITING_TRANSIT);
					delivery.deliverOrder(o);
				}
			} else if (o.getStatus() == OrderStatus.DELIVERED) {
				// iter.remove();
				// TODO add order average saving
			}
		}
	}

	/**
	 * Submits an order to the KitchenStation object for preparation and
	 * cooking, and begins to track the order.
	 */
	public void submitToKitchen(Order order) {
		orders.add(order);
		kitchen.prepareOrder(order);
	}

	/**
	 * Cancels the delivery of an order if possible. Cannot cancel if order is
	 * already being delivered.
	 * 
	 * @param order
	 *            - Order to cancel
	 * @return The success of the operation
	 */
	public boolean cancelOrder(Order order) {
		OrderStatus o = order.getStatus();
		if (o == OrderStatus.AWAITING_PICKUP || o == OrderStatus.DELIVERED
				|| o == OrderStatus.IN_TRANSIT) {
			return false;
		} else {
			orders.remove(order);
			return true;
		}
	}

	/**
	 * Loads previous OrderTracker data from the data file.
	 * 
	 * @throws IOException
	 *             - Error in reading from file.
	 */
	@Override
	public void loadFromFile() throws IOException {
		File cfile = new File(ORDER_FILE);
		Scanner scn = new Scanner(new FileInputStream(cfile));
		scn.useDelimiter(DataTracker.SEPARATOR);
		try {
			nextOrderID = scn.nextInt();
			chefs = scn.nextInt();
			ovens = scn.nextInt();
			ovenSpace = scn.nextInt();
			cars = scn.nextInt();
			isReadyToStart = true;
		} catch (NoSuchElementException nse) {
			throw new IOException("Corrupted file: " + ORDER_FILE);
		}
		scn.close();
	}

	/**
	 * Saves current next order ID # to data file.
	 * 
	 * @throws IOException
	 *             - Error in writing to file.
	 */
	@Override
	public void saveToFile() throws IOException {
		File cfile = new File(ORDER_FILE);
		FileWriter fw = new FileWriter(cfile);
		fw.write(nextOrderID + DataTracker.SEPARATOR);
		fw.write(chefs + DataTracker.SEPARATOR);
		fw.write(ovens + DataTracker.SEPARATOR);
		fw.write(ovenSpace + DataTracker.SEPARATOR);
		fw.write(cars + DataTracker.SEPARATOR);
		fw.close();
	}

	/**
	 * Returns a list of active orders.
	 * 
	 * @return a list of active orders.
	 */
	@Override
	public List<Order> getData() {
		return orders;
	}

	/**
	 * Adds an order to be tracked.
	 * 
	 * @param - data Order to be tracked.
	 */
	@Override
	public void addData(Order data) {
		submitToKitchen(data);
	}

	/**
	 * Returns the next order id.
	 * 
	 * @return the next order id.
	 */
	public static int getNextOrderID() {
		return nextOrderID++;
	}
}
