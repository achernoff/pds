package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import domain.Item;

/**
 * Maintains a list of available item options for ordering.
 * 
 * @author acc3863
 * 
 */
public class ItemTracker implements DataTracker<Item> {

	private static String ITEM_FILE = "items.dat";

	private List<Item> itemOptions;

	/**
	 * Creates an ItemTracker instance with no items tracked.
	 */
	public ItemTracker() {
		itemOptions = new ArrayList<Item>();
	}

	/**
	 * Loads items from the data file.
	 * 
	 * @throws IOException
	 *             - IOException occurred while reading items.dat.
	 */
	@Override
	public void loadFromFile() throws IOException {
		File cfile = new File(ITEM_FILE);
		Scanner scn = new Scanner(new FileInputStream(cfile));
		scn.useDelimiter(DataTracker.SEPARATOR);
		try {
		while (scn.hasNext()) {
			String name = scn.next();
			double price = scn.nextDouble();
			int prep = scn.nextInt();
			int cook = scn.nextInt();
			int space = scn.nextInt();
			Item i = new Item(name, price, prep, cook, space);
			addData(i);
		}
		} catch (NoSuchElementException nse) {
			throw new IOException("Corrupted file: " + ITEM_FILE);
		}
		scn.close();
	}

	/**
	 * Saves item to the data file.
	 * 
	 * @throws IOException
	 *             - IOException occured while reading items.dat.
	 */
	@Override
	public void saveToFile() throws IOException {
		File cfile = new File(ITEM_FILE);
		FileWriter fw = new FileWriter(cfile);
		for (Item i : itemOptions) {
			fw.write(i.getName() + DataTracker.SEPARATOR);
			fw.write(i.getPrice() + DataTracker.SEPARATOR);
			fw.write(i.getPrepTime() + DataTracker.SEPARATOR);
			fw.write(i.getCookTime() + DataTracker.SEPARATOR);
			fw.write(i.getOvenSpace() + DataTracker.SEPARATOR);
		}
		fw.close();
	}

	/**
	 * Returns the item options.
	 * 
	 * @return the item options.
	 */
	@Override
	public List<Item> getData() {
		return itemOptions;
	}

	/**
	 * Adds an item to the item options if the item is not already contained.
	 */
	@Override
	public void addData(Item data) {
		if (!itemOptions.contains(data))
			itemOptions.add(data);
	}
	
}
