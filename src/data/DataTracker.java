package data;

import java.io.IOException;
import java.util.List;

/**
 * DataTracker generalizes several types of data the PDS needs to keep track of.
 * Such as Customers, Items, and Orders.
 * 
 * @author acc3863
 * 
 * @param <T>
 *            - The type of data to track.
 */
public interface DataTracker<T> {
	
	static String SEPARATOR = "~~";
	
	/**
	 * Loads data from a file.
	 * 
	 * @throws IOException
	 *             - Data could not be loaded.
	 */
	public void loadFromFile() throws IOException;

	/**
	 * Saves data to a file.
	 * 
	 * @throws IOException
	 *             - Data could not be saved.
	 */
	public void saveToFile() throws IOException;

	/**
	 * Returns a list of the data being tracked.
	 * 
	 * @return a list of the data being tracked.
	 */
	public List<T> getData();

	/**
	 * Adds data to be tracked.
	 * 
	 * @param data
	 *            - Data object to be tracked.
	 */
	public void addData(T data);
}
