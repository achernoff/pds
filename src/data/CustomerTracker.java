package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import domain.Customer;

/**
 * Tracks known customer information and saves it to file.
 * 
 * @author acc3863
 * 
 */
public class CustomerTracker implements DataTracker<Customer> {
	
	private static String CUSTOMER_FILE = "customers.dat";
	
	private List<Customer> customers;

	/**
	 * Maintains a list of known customers that have ordered in the past.
	 */
	public CustomerTracker() {
		this.customers = new ArrayList<Customer>();
	}

	/**
	 * Loads previous customer data from the customer file.
	 * 
	 * @throws IOException
	 *             - IOException occurred while reading customers.dat.
	 */
	@Override
	public void loadFromFile() throws IOException {
		File cfile = new File(CUSTOMER_FILE);
		Scanner scn = new Scanner(new FileInputStream(cfile));
		scn.useDelimiter(DataTracker.SEPARATOR);
		try {
		while (scn.hasNext()) {
			String name = scn.next();
			String phone = scn.next();
			Customer i = new Customer(name, phone);
			addData(i);
		}
		} catch (NoSuchElementException nse) {
			throw new IOException("Corrupted file: " + CUSTOMER_FILE);
		}
		scn.close();
	}

	/**
	 * Saves all customers to the customer data file.
	 */
	@Override
	public void saveToFile() throws IOException {
		File cfile = new File(CUSTOMER_FILE);
		FileWriter fw = new FileWriter(cfile);
		for (Customer i : customers) {
			fw.write(i.getName() + DataTracker.SEPARATOR);
			fw.write(i.getPhone() + DataTracker.SEPARATOR);
		}
		fw.close();
	}

	/**
	 * Returns the list of customers.
	 * 
	 * @return the list of customers.
	 */
	@Override
	public List<Customer> getData() {
		return customers;
	}

	/**
	 * Adds a customer to the list of customers.
	 */
	@Override
	public void addData(Customer cust) {
		customers.add(cust);
	}

}
