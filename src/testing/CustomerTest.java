package testing;

import domain.Customer;
import junit.framework.TestCase;

public class CustomerTest extends TestCase {

	/**
	 * Testing for Customer Class
	 */
	
	/**
	 * Testing for toString return
	 */
	public void testToString() {
		Customer testC1 = new Customer("Alex", "7543997"); //Testing Normal name + normal phone
		Customer testC2 = new Customer("John", "0"); //Testing normal name + bad phone
		Customer testC3 = new Customer("a", "5857549887"); //Testing 1 letter name + normal phone 
		assertEquals("Customer C1 is Alex 7543997", testC1.toString(), "Alex(7543997)");
		assertEquals("Customer C2 is John 0", testC2.toString(), "John(0)");
		assertEquals("Customer C3 is a 5857549887", testC3.toString(), "a(5857549887)");
		
	}

	/**
	 * Testing for getting name return.
	 */
	public void testGetName() {
		Customer testC1 = new Customer("Alex", "7543997"); //Testing Normal name + normal phone
		Customer testC2 = new Customer("John", "0"); //Testing normal name + bad phone
		Customer testC3 = new Customer("a", "5857549887"); //Testing 1 letter name + normal phone 
		assertEquals("Customer C1 is Alex", testC1.getName(), "Alex");
		assertEquals("Customer C2 is John", testC2.getName(), "John");
		assertEquals("Customer C3 is a", testC3.getName(), "a");
		
		
	}

	/**
	 * Testing for getting Phone number return.
	 */
	public void testGetPhone() {
		Customer testC1 = new Customer("Alex", "7543997"); //Testing Normal name + normal phone
		Customer testC2 = new Customer("John", "0"); //Testing normal name + bad phone
		Customer testC3 = new Customer("a", "5857549887"); //Testing 1 letter name + normal phone 
		assertEquals("Customer C1 #is 7543997", testC1.getPhone(), "7543997");
		assertEquals("Customer C2 #is 0", testC2.getPhone(), "0");
		assertEquals("Customer C3 #is 5857549887", testC3.getPhone(), "5857549887");
		
	}
	
}
