package testing;

import java.util.ArrayList;
import java.util.List;

import domain.*;
import junit.framework.TestCase;

public class OrderTest extends TestCase {

	/**
	 * Test class for the Order Class. Creates a customer, destination and Item
	 * List (2 items) The order is Instantiated.
	 * 
	 * Test 1 - Getting list of items; Test 2- Adding item, and getting new list
	 * of items. Test 3 - Getting Customer. Test 4 - Getting Destination. Test 5
	 * - Getting Status (by default is IN PREP). Test 6 - Setting new status
	 * DELIVERED and checking new status.
	 */
	public void testOrder() {
		Customer c1 = new Customer("Geronimo", "5857548778");
		Destination d1 = Destination.RIT;
		List<Item> itemList = new ArrayList<Item>() {
			{
				add(new Item("Pizza", 5, 5, 10, 4));
				add(new Item("Pizza Logs", 5, 0, 6, 4));
			}
		};
		Order order1 = new Order(itemList, c1, d1);
		assertEquals("Getting list of added items", order1.getItems(), itemList);
		order1.getItems().add(new Item("Salad", 5, 5, 0, 0)); // Tested for Item
																// Add
		assertEquals("Adding new item to Order Item List", order1.getItems(),
				itemList);
		assertEquals("Getting Customer from Order", order1.getCustomer(), c1);
		assertEquals("Getting Destination from Order", order1.getDestination(),
				d1);
		assertEquals("Getting INITIAL Status from order", order1.getStatus(),
				OrderStatus.IN_PREP);
		order1.setStatus(OrderStatus.DELIVERED);
		assertEquals("Changed Status, getting new Status", order1.getStatus(),
				OrderStatus.DELIVERED);

	}

}
