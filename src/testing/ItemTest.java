package testing;

import domain.Item;
import junit.framework.TestCase;

/**
 * Testing the Item class will be more applicable once we start imputing from
 * the data files. At that point we will need to place some assertion statements
 * to ensure that the data inputed from the files is applicable (e.g. that there
 * are no negative cook times, etc.)
 * 
 * Until then we are just testing whether we get out what we put in - which
 * gives us no information.
 * 
 * 
 */

public class ItemTest extends TestCase {

	public void testGetCookTime() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 5, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("Cook Time for C1", testC1.getCookTime(), 10);
		assertEquals("Cook Time for C2", testC2.getCookTime(), 15);
		assertEquals("Cook Time for C3", testC3.getCookTime(), 0);

	}

	public void testToString() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 5, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("String for C1", testC1.toString(), "Pizza Logs");
		assertEquals("String for C2", testC2.toString(), "Medium Pizza");
		assertEquals("String for C3", testC3.toString(), "Tossed Salad");
	}

	public void testGetName() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 5, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("Name for C1", testC1.getName(), "Pizza Logs");
		assertEquals("Name for C2", testC2.getName(), "Medium Pizza");
		assertEquals("Name for C3", testC3.getName(), "Tossed Salad");
	}

	public void testGetOvenSpace() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 4, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("Oven Space for C1", testC1.getOvenSpace(), 1);
		assertEquals("Oven Space for C2", testC2.getOvenSpace(), 2);
		assertEquals("Oven Space for C3", testC3.getOvenSpace(), 0);
	}

	public void testGetPrepTime() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 5, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("Prep Time for C1", testC1.getPrepTime(), 0);
		assertEquals("Prep Time for C2", testC2.getPrepTime(), 10);
		assertEquals("Prep Time for C3", testC3.getPrepTime(), 5);
	}

	public void testIsPrepared() {
		Item testC1 = new Item("Pizza Logs", 5, 0, 10, 1);
		Item testC2 = new Item("Medium Pizza", 5, 10, 15, 2);
		Item testC3 = new Item("Tossed Salad", 5, 5, 0, 0);
		assertEquals("Is Prepared for C1", testC1.isPrepared(), false);
		assertEquals("Is Prepared for C2", testC2.isPrepared(), false);
		assertEquals("Is Prepared for C3", testC3.isPrepared(), false);
	}
}
