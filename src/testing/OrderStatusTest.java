package testing;

import junit.framework.TestCase;
import domain.*;

public class OrderStatusTest extends TestCase {

	/**
	 * Trivial Test Case for Order Status Enum
	 */
	public void testOrderStatus() {
		assertTrue(OrderStatus.IN_PREP != null);
		assertTrue(OrderStatus.AWAITING_TRANSIT!=null);
		assertTrue(OrderStatus.IN_TRANSIT!=null);		
		assertTrue(OrderStatus.AWAITING_PICKUP!=null);
		assertTrue(OrderStatus.DELIVERED!=null);
	}

}
