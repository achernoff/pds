package testing;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import data.OrderTracker;
import domain.*;
import junit.framework.TestCase;

public class OrderTrackerTest extends TestCase {

	/**
	 * Testing an orderTracker handling 1 order
	 */
	public void testOrderTracker1() {
		// Creating order 1
		Customer c1 = new Customer("Geronimo", "5857548778");
		Destination d1 = Destination.RIT;
		List<Item> itemList = new ArrayList<Item>() {
			{
				add(new Item("Pizza", 5, 5, 10, 4));
				add(new Item("Pizza Logs", 5, 0, 6, 4));
			}
		};
		Order order1 = new Order(itemList, c1, d1);
		// Created order 1

		OrderTracker ot = new OrderTracker();
		ot.initStations(1, 1, 10, 1);

		ot.startDay();
		assertTrue(ot.isDayRunning()); // Test if the ticker has begun and timer
										// has
										// started
		ot.submitToKitchen(order1);
		JFrame j = new javax.swing.JFrame("Test1");
		j.setVisible(true);
		assertTrue(!ot.getData().isEmpty()); // Test to see if order is part of
		assertTrue(ot.getData().size() == 1); // orders list
		System.out.println("******** Finished OrderTrackerTest #1 ********");
		j.setVisible(false);
		j.dispose();
	}

	/**
	 * Testing an orderTracker handling 2 orders
	 */
	public void testOrderTracker2() {
		// Creating 2 orders
		Destination d = Destination.RIT;
		Destination x = Destination.UNIVERSITY_OF_ROCHESTER; // 2nd Order Test
		Customer c = new Customer("Alex", "5454545");
		Customer y = new Customer("Abdul", "2342343"); // 2nd Order Test
		ArrayList<Item> z = new ArrayList<Item>() { // 2nd Order Test
			{
				add(new Item("Large Pizza", 5, 15, 20, 4));
				add(new Item("Small Pizza", 5, 8, 13, 1));
			}
		};
		ArrayList<Item> a = new ArrayList<Item>() {
			{
				add(new Item("Medium Pizza", 5, 10, 13, 2));
				add(new Item("Tossed Salad", 5, 5, 0, 0));
				add(new Item("Pizza Log", 5, 0, 15, 1));
			}
		};
		Order o = new Order(a, c, d);
		Order w = new Order(z, y, x);
		// 2 orders Created

		OrderTracker ot2 = new OrderTracker();// Instantiating ordertracker
		ot2.initStations(1, 1, 10, 1);		
		
		assertFalse(ot2.isDayRunning()); // Test for Day has not started yet
		ot2.startDay();
		assertTrue(ot2.isDayRunning()); // Test if the ticker has begun and
										// timer has
		// started

		ot2.submitToKitchen(o);
		assertTrue(ot2.getData().size() == 1);
		ot2.submitToKitchen(w);
		assertTrue(ot2.getData().size() == 2);
		JFrame j = new javax.swing.JFrame("Test2");
		j.setVisible(true);
		System.out.println("******** Finished OrderTrackerTest #2 ********");
		j.setVisible(false);
		j.dispose();
	}

	public void testOrderTracker3() {
		// Creating 3 orders
		Destination d1 = Destination.RIT;
		Destination d2 = Destination.UNIVERSITY_OF_ROCHESTER;
		Destination d3 = Destination.MCC;
		Customer c1 = new Customer("Alex", "5454545");
		Customer c2 = new Customer("Abdul", "2342343");
		Customer c3 = new Customer("Tom", "1234567");
		ArrayList<Item> i1 = new ArrayList<Item>() { 
			{
				add(new Item("Large Pizza", 5, 15, 20, 4));
				add(new Item("Small Pizza", 5, 8, 13, 1));
			}
		};
		ArrayList<Item> i2 = new ArrayList<Item>() {
			{
				add(new Item("Medium Pizza", 5, 10, 13, 2));
				add(new Item("Tossed Salad", 5, 5, 0, 0));
				add(new Item("Pizza Log", 5, 0, 15, 1));
			}
		};
		ArrayList<Item> i3 = new ArrayList<Item>() {
			{
				add(new Item("Medium Pizza", 5, 10, 13, 2));
				add(new Item("Tossed Salad", 5, 5, 0, 0));
				add(new Item("Pizza Log", 5, 0, 15, 1));
				add(new Item("Large Pizza", 5, 15, 20, 4));
			}
		};
		Order o1 = new Order(i1, c1, d1);
		Order o2 = new Order(i2, c2, d2);
		Order o3 = new Order(i3, c3, d3);

		OrderTracker ot2 = new OrderTracker();// Instantiating OrderTracker
		ot2.initStations(1, 1, 10, 1);
		assertFalse(ot2.isDayRunning()); // Test for Day has not started yet
		ot2.startDay();
		assertTrue(ot2.isDayRunning()); // Test if the ticker has begun and
										// timer has
		// started

		ot2.submitToKitchen(o1);
		assertTrue(ot2.getData().size() == 1);
		ot2.submitToKitchen(o2);
		assertTrue(ot2.getData().size() == 2);
		ot2.submitToKitchen(o3);
		assertTrue(ot2.getData().size() == 3);
		JFrame j = new javax.swing.JFrame("Test2");
		j.setVisible(true);
		System.out.println("******** Finished OrderTrackerTest #3 ********");
		j.setVisible(false);
		j.dispose();
	}
}
