package testing;

import domain.Destination;
import junit.framework.TestCase;

/**
 * Testing the Destination class will be more applicable once we start imputing
 * from the data files. At that point we will need to place some assertion
 * statements to ensure that the data inputed from the files is applicable (e.g.
 * that drivers have a name, that timeTo is not a negative number, etc.)
 * 
 * Until then we are just testing whether we get out what we put in - which
 * gives us no information.
 * 
 * @author cpk4877
 * 
 */

public class DestinationTest extends TestCase {

	public void testToString() {
		Destination testD1 = Destination.RIT;
		Destination testD2 = Destination.UNIVERSITY_OF_ROCHESTER;
		Destination testD3 = Destination.NAZARETH_COLLEGE;
		assertEquals("Destination String for D1", testD1.toString(),
				"RIT-- Time To: 10, Coll. Time: 5");
		assertEquals("Destination String for D2", testD2.toString(),
				"University of Rochester-- Time To: 15, Coll. Time: 5");
		assertEquals("Destination String for D3", testD3.toString(),
				"Nazareth College-- Time To: 12, Coll. Time: 3");
	}

	public void testGetName() {
		Destination testD1 = Destination.RIT;
		Destination testD2 = Destination.UNIVERSITY_OF_ROCHESTER;
		Destination testD3 = Destination.NAZARETH_COLLEGE;
		assertEquals("Get Name for D1:", testD1.getName(), "RIT");
		assertEquals("Get Name for D2:", testD2.getName(),
				"University of Rochester");
		assertEquals("Get Name for D3:", testD3.getName(), "Nazareth College");
	}

	public void testTimeTo() {
		Destination testD1 = Destination.RIT;
		Destination testD2 = Destination.UNIVERSITY_OF_ROCHESTER;
		Destination testD3 = Destination.NAZARETH_COLLEGE;
		assertEquals("Get TimeTo for D1:", testD1.getTimeTo(), 10);
		assertEquals("Get TimeTo for D2:", testD2.getTimeTo(), 15);
		assertEquals("Get TimeTo for D3:", testD3.getTimeTo(), 12);
	}

	public void testCollectionTime() {
		Destination testD1 = Destination.RIT;
		Destination testD2 = Destination.UNIVERSITY_OF_ROCHESTER;
		Destination testD3 = Destination.NAZARETH_COLLEGE;
		assertEquals("Get CollectionTime for D1:", testD1.getCollectionTime(),
				5);
		assertEquals("Get CollectionTime for D2:", testD2.getCollectionTime(),
				5);
		assertEquals("Get CollectionTime for D3:", testD3.getCollectionTime(),
				3);
	}
}
