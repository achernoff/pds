package domain;


import java.text.DecimalFormat;

/**
 * The Item class describes and updates a component of an order.
 * 
 * @author acc3863
 * 
 */
public class Item implements Cloneable {

	private String name;
	private int prepTime;
	private int cookTime;
	private int ovenSpace; // TODO: Should check if ovenSpace > Oven capacity
	private boolean isPrepared;
	private double price;

	/**
	 * Initializes an item with a name, preparation time, cook time and how much
	 * oven space the item requires.
	 * 
	 * @param name
	 *            - The name
	 * @param prepTime
	 *            - Amount of time for a chef to prepare
	 * @param cookTime
	 *            - Amount of time needed to cook (0 if no cooking needed)
	 * @param ovenSpace
	 *            - Amount of space needed in an oven to cook (0 if no cooking
	 *            needed)
	 */
	public Item(String name, double price, int prepTime, int cookTime,
			int ovenSpace) {
		this.name = name;
		this.price = price;
		this.cookTime = cookTime;
		this.prepTime = prepTime;
		this.ovenSpace = ovenSpace;
		this.isPrepared = false;
	}

	/**
	 * Constructs an instance of Item as a clone of another Item.
	 * 
	 * @param other
	 *            - Item to clone.
	 */
	private Item(Item other) {
		this(other.name, other.price, other.prepTime, other.cookTime,
				other.ovenSpace);
	}

	/**
	 * Used by a KitchenStation object to perform the necessary procedures to
	 * fully prepare this item for delivery.
	 */
	public void process() {
		// To be implemented later.
		throw new UnsupportedOperationException("Operation not implemented.");
	}

	/**
	 * Returns a string describing this item in the following format.
	 * "Item: <name>, Prepared <isPrepared>"
	 * 
	 * @return String describing this item.
	 */
	@Override
	public String toString() {
		return name + " " + DecimalFormat.getCurrencyInstance().format(price);
	}

	/**
	 * Query method to get the name of the item
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the price.
	 * 
	 * @return the price.
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Query method to get the time required to prep
	 * 
	 * @return the prepTime
	 */
	public int getPrepTime() {
		return prepTime;
	}

	/**
	 * Query method to get the time required to cook
	 * 
	 * @return the cookTime
	 */
	public int getCookTime() {
		return cookTime;
	}

	/**
	 * Query method to get the oven capacity/space
	 * 
	 * @return the ovenSpace
	 */
	public int getOvenSpace() {
		return ovenSpace;
	}

	/**
	 * Query method to see if item is prepared
	 * 
	 * @return the isPrepared
	 */
	public boolean isPrepared() {
		return isPrepared;
	}

	/**
	 * Set the isPrepared boolean in the Item
	 * 
	 * @param isPrepared
	 */
	public void setPrepared(boolean isPrepared) {
		this.isPrepared = isPrepared;
	}

	/**
	 * Clones this item, returning an exact copy. Recommended to clone items
	 * before using them in orders.
	 * 
	 * @return The cloned item.
	 */
	@Override
	public Item clone() {
		return new Item(this);
	}

	/**
	 * Tests to see if this object equals another.
	 * 
	 * @return True if they are equal.
	 */
	@Override
	public boolean equals(Object o) {
		try {
			Item i = (Item) o;
			return i.cookTime == this.cookTime
					&& i.name.equals(this.name)
					&& i.ovenSpace == this.ovenSpace
					&& i.prepTime == this.prepTime
					&& i.price == this.price;
		} catch (ClassCastException cce) {
			return false;
		}
	}
}
