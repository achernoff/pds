package domain;


public class Job<T> {
	public T object;
	public int timeRemaining;
	public int space;

	/**
	 * Job Constructor.
	 * Work is imposed against a Job<T> Object which has the parameter
	 * timeRemaning to see how much more seconds of work it requires.
	 * The instance doing work is an instance of LimitedJobSchedule, whose
	 * tick method decreases the timeRemaining by one. 
	 * 
	 * @param object - the actual object to do work on ie. Item Object.
	 * @param timeRemaining - time required to finish the Job. (prep, cook, deliver)
	 * @param space - the amount of space it occupies 
	 */
	public Job(T object, int timeRemaining, int space) {
		this.object = object;
		this.timeRemaining = timeRemaining;
		this.space = space;
	}
}
