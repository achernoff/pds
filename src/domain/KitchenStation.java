package domain;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The KitchenStation class simulates the operations of the kitchen in the pizza
 * delivery system.
 * 
 * @author acc3863
 * 
 */
public class KitchenStation {

	private LimitedJobSchedule<Item> preps;
	private LimitedJobSchedule<Item> ovens;
	private Logger log;

	/**
	 * 
	 * Constructor method for a KitchenStation class. The generic class Job<T>
	 * is instantiated in order for ArrayList to handle them
	 * 
	 * Precondition: have integer values for amount or perparers, numOvens,
	 * ovenSpace. Can be set before ticker.
	 * 
	 * Postcondition: KitchenStation is instantiated.
	 * 
	 * @param numCars
	 *            - The number of delivery people (deliverers)
	 * 
	 * @param preparers
	 *            - Number of cooks that can prepare
	 * @param numOvens
	 *            - number of Ovens, each with their own oven space
	 * @param ovenSpace
	 *            - The oven space of a particular oven
	 */
	public KitchenStation(int preparers, int numOvens, int ovenSpace) {
		this.preps = new LimitedJobSchedule<Item>(Item.class, preparers, 1);
		this.ovens = new LimitedJobSchedule<Item>(Item.class, numOvens,
				ovenSpace);
		this.log = Logger.getLogger("PDS.Kitchen");
	}

	/**
	 * Method to start the preparation process on an order. Adds every item in
	 * the order to the preparation queue.
	 * 
	 * Precondition: Order is completely instantiated and read for preparation.
	 * 
	 * Postcondition: each item in the order is set as a job and scheduled to
	 * prepare. Status logs are logged to the log.
	 * 
	 * @param order
	 *            - The order to prepare.
	 */
	public void prepareOrder(Order order) {
		for (Item i : order.getItems()) {
			Job<Item> job = new Job<Item>(i, i.getPrepTime(), 1);
			preps.schedule(job);
			log.log(Level.FINEST, "Queued for prep: " + i.getName());
		}
	}

	/**
	 * Tick method is used to query every second. Postcondition: for every job
	 * ready to go to the oven, the method will schedule it.
	 * 
	 * Precondition: day has started and ticker has begun.
	 * 
	 * Postcondition: Will return a list of items that have completed the
	 * cooking process (either not required by item or have been cooking for
	 * desired amount of time). If not, the items will be scheduled for the oven.
	 * All the necessary boolean variables for item are changed.
	 * 
	 * @return List of items the oven has finished cooking.
	 */
	public List<Item> tick() {
		List<Item> cooked = new ArrayList<Item>();
		for (Item i : preps.tick()) {
			if (i.getCookTime() == 0) {
				cooked.add(i); // doesn't need cooking
			} else {
				Job<Item> job = new Job<Item>(i, i.getCookTime(),
						i.getOvenSpace());
				ovens.schedule(job);
				log.log(Level.FINEST, "Queued for ovens: " + i.getName());
			}
		}
		cooked.addAll(ovens.tick());
		for (Item i : cooked) {
			i.setPrepared(true);
			log.log(Level.FINEST, "Finished: " + i.getName());
		}
		return cooked;
	}
}
