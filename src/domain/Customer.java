package domain;


/**
 * The customer clsass embodies a customer customer who ordered from
 * from the delivery company.
 * 
 * @author acc3863
 * 
 */
public class Customer {

	private String name;
	private String phone;

	/**
	 * Constructs a customer object with a name and a phone number.
	 * 
	 * @param name
	 *            - The customer's name.
	 * @param phone
	 *            - The customer's phone number.
	 */
	public Customer(String name, String phone) {
		this.name = name;
		this.phone = phone;
		if(! phone.matches("((\\d(-?))?(\\d){3}(-?))?((\\d){3}-?(\\d){4})")) {
			throw new IllegalArgumentException("Bad phone number");
		}
	}

	/**
	 * Returns a string describing this customer in the following format.
	 * "<name> <phone #>"
	 * 
	 * @return String describing this customer.
	 */
	@Override
	public String toString() {
		return name + "(" + phone + ")";
	}

	/**
	 * @return the name of Customer
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the phone # of Customer
	 */
	public String getPhone() {
		return phone;
	}

}
