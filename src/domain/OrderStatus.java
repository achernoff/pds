package domain;


/**
 * The OrderStatus enum describes all possible states an order may be in during
 * its progress through the delivery process.
 * 
 * @author acc3863
 * 
 */
public enum OrderStatus {
	IN_PREP("in preparation"), AWAITING_TRANSIT("awaiting transit"), IN_TRANSIT(
			"in transit"), AWAITING_PICKUP("awaiting pickup"), DELIVERED(
			"delivered");

	private String name;

	private OrderStatus(String name) {
		this.name = name;
	}

	/**
	 * Method that gets name, which is a status in this case.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
}
