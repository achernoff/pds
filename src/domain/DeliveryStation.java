package domain;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The DeliveryStation class simulates the operations of the delivery cars in
 * the Pizza Delivery System.
 * 
 * @author acc3863
 * 
 */
public class DeliveryStation {

	private LimitedJobSchedule<Order> cars;
	private Logger log;

	/**
	 * Constructor method for a DeliveryStation class.
	 * 
	 * Precondition: numCars is an int, and has been stored as input to
	 * instantiate a new DeliveryStation.
	 * 
	 * Postcondition: DeliveryStation is instantiated with an instance of
	 * LimitedJobSchedule consisting of cars, and log, used for CLI and logging
	 * purposes.
	 * 
	 * @param numCars
	 *            - The number of delivery people (deliverers)
	 */
	public DeliveryStation(int numCars) {
		cars = new LimitedJobSchedule<Order>(Order.class, numCars, 1);
		log = Logger.getLogger("PDS.Delivery");
	}

	/**
	 * Method that begins delivery process. New Job<Order> Generic is
	 * instantiated with appropriate parameters and then sent to scheduling.
	 * 
	 * Precondition: Have a non-null order ready to deliver, have available
	 * deliverer.
	 * 
	 * Postcondition: a specific order begins the delivery process by making a
	 * new Job<order> that is constructed to do work on. The amount of work
	 * consists of time to the delivery place, time for customer to retrieve
	 * pizza, and the way back. Logs actions.
	 * 
	 * @param order
	 *            - the order that is being delivered. (items, destination,
	 *            etc.)
	 * 
	 */
	public void deliverOrder(Order order) {
		Job<Order> job = new Job<Order>(order, 2
				* order.getDestination().getTimeTo()
				+ order.getDestination().getCollectionTime(), 1);
		cars.schedule(job);
		log.log(Level.FINEST, "Queued: " + order.toString());
	}

	/**
	 * Tick is the frequent (every 1000 ms) query for the state of the delivery
	 * process.
	 * 
	 * Precondition: the day has started, which means tick function is ticking
	 * every XXXXms.
	 * 
	 * Postconditions: Depending on the time elapsed per order, tick will query
	 * and change the order status. List<order> is returned. Log information is
	 * sent to the logger.
	 * 
	 * @return List<order>
	 */
	public List<Order> tick() {
		List<Order> delivered = cars.tick(); // tick the cars
		for (List<Job<Order>> i : cars.getSlots()) { // see where cars are along
														// their delivery route
			for (Job<Order> j : i) {
				Order order = j.object;
				int tTotal = 2 * order.getDestination().getTimeTo()
						+ order.getDestination().getCollectionTime();
				int tColl = order.getDestination().getCollectionTime();
				int tTo = order.getDestination().getTimeTo();
				// set order status accordingly
				if (j.timeRemaining < tTotal - tTo - tColl) {
					if (order.getStatus() != OrderStatus.DELIVERED) {
						log.log(Level.FINEST, "Delivered: " + order.toString());
					}
					order.setStatus(OrderStatus.DELIVERED);
				} else if (j.timeRemaining < tTotal - tTo) {
					if (order.getStatus() != OrderStatus.AWAITING_PICKUP) {
						log.log(Level.FINEST,
								"Waiting for pickup on: " + order.toString());
					}
					order.setStatus(OrderStatus.AWAITING_PICKUP);
				} else {
					if (order.getStatus() != OrderStatus.IN_TRANSIT
							&& order.getStatus() != OrderStatus.IN_PREP) {
						log.log(Level.FINEST, "Delivering: " + order.toString());
					}
					order.setStatus(OrderStatus.IN_TRANSIT);
				}
			}
		}
		return delivered;
	}

}
