package domain;


/**
 * The destination class describes a destination to deliver to. Contains needed
 * information to deliver an order.
 * 
 * @author acc3863
 * 
 */
public enum Destination {

	RIT("RIT", 10, 5), UNIVERSITY_OF_ROCHESTER("University of Rochester", 15, 5), NAZARETH_COLLEGE(
			"Nazareth College", 12, 3), ST_JOHN_FISHER("St. John Fisher", 17, 3), ROBERTS_WESLEYAN(
			"Roberts Wesleyan", 20, 2), MCC("MCC", 17, 6);

	private String name;
	private int timeTo;
	private int collectionTime;

	/**
	 * Constructs a destination object with a name, travel time, and collection
	 * time.
	 * 
	 * @param name
	 *            - The name of the destination
	 * @param timeTo
	 *            - The time to drive there.
	 * @param collectionTime
	 *            - The time to let the customer collect their order.
	 * 
	 *            Note: The success of events are as follows; timeTo,
	 *            collectionTime, timeTo(timeBack).
	 */
	private Destination(String name, int timeTo, int collectionTime) {
		this.name = name;
		this.timeTo = timeTo;
		this.collectionTime = collectionTime;
		
	}

	/**
	 * Returns a string describing this destination in the following format.
	 * "<name> -- Time To: <timeTo>, Coll. Time: <collectionTime>"
	 * 
	 * @return Returns a string describing this destination.
	 */
	public String toString() {
		return name + "-- Time To: " + timeTo + ", Coll. Time: "
				+ collectionTime;
	}

	/**
	 * Query method to get the Destination name
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Query method to get the time required to get to destination. TimeTo is
	 * also the same as time required to go back.
	 * 
	 * @return the timeTo
	 */
	public int getTimeTo() {
		return timeTo;
	}

	/**
	 * Query method to get the time required to collect money and deliver pizza
	 * 
	 * @return the collectionTime
	 */
	public int getCollectionTime() {
		return collectionTime;
	}

}
