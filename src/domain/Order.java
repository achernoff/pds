package domain;


import java.util.List;

import data.OrderTracker;import domain.Item;


/**
 * The Order class describes and provides access to all information regarding a
 * specific food order to be delivered to a customer.
 * 
 * @author acc3863
 * 
 */
public class Order {
	private List<Item> items;
	private Customer customer;
	private Destination destination;
	private OrderStatus status;
	private int id;

	/**
	 * Constructs an order with a list of items on the order, along with a
	 * customer and location to deliver to.
	 * 
	 * Order Object holds: items, customer, destination. Provides information
	 * such as its own respective ID, and a status.
	 * 
	 * @param items
	 *            - Items to be prepared.
	 * @param customer
	 *            - Customer to deliver to.
	 * @param destination
	 *            - Location to deliver to.
	 */
	public Order(List<Item> items, Customer customer, Destination destination) {
		this.id = OrderTracker.getNextOrderID();
		this.items = items;
		this.customer = customer;
		this.destination = destination;
		this.status = OrderStatus.IN_PREP;
	}

	/**
	 * Returns the items within a specific order
	 * 
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * Returns the customer within a specific order
	 * 
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * Returns the destination within a specific order
	 * 
	 * @return the destination
	 */
	public Destination getDestination() {
		return destination;
	}

	/**
	 * Returns the status within a specific order
	 * 
	 * @return the status
	 */
	public OrderStatus getStatus() {
		return status;
	}

	/**
	 * Sets the order's status within a specific order
	 * 
	 * @param status
	 */
	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	/**
	 * Returns the id of a specific order
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the total price.
	 * 
	 * @return the total price.
	 */
	public double getPrice() {
		double sum = 0;
		for (Item i : items) {
			sum += i.getPrice();
		}
		return sum;
	}

	/**
	 * Returns a string representing the order.
	 * 
	 * @return a string representing the order.
	 */
	public String toString() {
		return String.format("Order #%1d to %2s at %3s", id,
				customer.toString(), destination.getName());
	}
}
