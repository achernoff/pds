package domain;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * The LimitedJobSchedule class models an environment where workers perform work
 * on jobs (jobs are of type T). Each worker does 1 unit of work on a Job T for
 * every tick. Tick is set to perform for a specific interval (standard 1000ms =
 * 1second). Each worker has a variable called slotCapacity, consisting of the
 * amount of Jobs T it can handle.
 * 
 * @author acc3863
 * 
 */
public class LimitedJobSchedule<T> {

	private List<List<Job<T>>> slots;
	private int slotCapacity;

	/**
	 * Constructor for a LimitedJobSchedule class.
	 * 
	 * @param clazz
	 *            - The type of jobs this object will handle.
	 * @param workSlots
	 *            - The number of slots that do work on jobs.
	 * @param slotCapacity
	 *            - The maximum load capacity of each work slot.
	 * @SuppressWarnings - Guaranteeing effective and legal execution to
	 *                   compiler
	 */
	public LimitedJobSchedule(Class<T> clazz, int workSlots, int slotCapacity) {
		this.slots = new ArrayList<List<Job<T>>>();
		for (int i = 0; i < workSlots; i++)
			this.slots.add(Collections
					.synchronizedList(new ArrayList<Job<T>>()));
		// this.slots.add(new ArrayList<Job<T>>());
		this.slotCapacity = slotCapacity;
	}

	/**
	 * Returns the work slots. See topmost Javadocs for additional information
	 * on slots
	 * 
	 * @return the work slots.
	 */
	public List<List<Job<T>>> getSlots() {
		return slots;
	}

	/**
	 * Simulates one discrete instance of time passing. Each worker will
	 * sequentially perform work on job objects until their load is maximized.
	 * Checks to see if any jobs are finished. If so, removes them from the work
	 * queues and returns them.
	 * 
	 * @return list of finished job encapsulated objects.
	 */
	public List<T> tick() {
		synchronized (slots) {
			for (List<Job<T>> queue : slots) { // iterate through work slots
				int usedLoad = 0;
				for (Job<T> job : queue) { // iterate through a slot's jobs
					if (job.timeRemaining > 0
							&& usedLoad + job.space <= slotCapacity) {
						// see if slot can handle any more jobs
						job.timeRemaining--; // do work
						usedLoad += job.space; // increment usedLoad
					} else {
						break; // go to next slot
					}
				}
			}
		}
		// now that work has been done, remove and return finished jobs
		List<T> finished = new ArrayList<T>();
		for (List<Job<T>> queue : slots) {
			Iterator<Job<T>> iter = queue.listIterator();
			while (iter.hasNext()) {
				Job<T> j = iter.next();
				if (j.timeRemaining <= 0) {
					iter.remove();
					finished.add(j.object);
				}
			}
		}

		return finished;
	}

	/**
	 * Adds a Job object to a work slot's queue. Tries to add in such a way so
	 * that the job will be completed as fast as possible, without delaying
	 * already-queued jobs.
	 * 
	 * Precondition: have an intantiated Job Object.
	 * 
	 * Postcondition: adds Job object to slot queue.
	 * 
	 * @param job
	 *            - The job to schedule.
	 */
	public void schedule(Job<T> job) {
		synchronized (slots) {
			boolean scheduled = false;
			// look for a spot until we find one
			// increment test time
			for (int tCount = 0; !scheduled; tCount++) {
				// get how much load is available at a certain time in the
				// future
				int[] loadArray = loadForTime(tCount);
				// see if the job fits
				for (int i = 0; i < loadArray.length; i++) {
					// if it fits, queue the job and return
					if (loadArray[i] >= job.space) {
						slots.get(i).add(job);
						// System.out.println(String.format("Scheduling %1s in slot %2d at position %3d",
						// job.object.toString(), i, slots.get(i).size()));
						scheduled = true;
						break;
					}
				}
			}
		}
	}

	/**
	 * Function to determine how much load is free at a certain offset of ticks
	 * into the future. This is the method used to optimally fit jobs within a
	 * given slot capacity and a given time remaining.
	 * 
	 * @param t
	 *            - Ticks in the future.
	 * @return Array of available load corresponding to work slots.
	 */
	private int[] loadForTime(int t) {
		int[] spaceArray = new int[slots.size()];
		// iterate through slots
		for (int i = 0; i < slots.size(); i++) {
			int availableSpace = slotCapacity;
			// iterate through all jobs in slot
			for (Job<T> job : slots.get(i)) {
				// if a job will be running and will not have finished,
				// decrement available space
				if (availableSpace - job.space >= 0) {
					if (job.timeRemaining >= t) {
						availableSpace -= job.space;
					}
				}
			}
			spaceArray[i] = availableSpace;
		}
		return spaceArray;
	}
}
