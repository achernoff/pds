package cli;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import data.CustomerTracker;
import data.ItemTracker;
import data.OrderTracker;
import domain.Customer;
import domain.Destination;
import domain.Item;
import domain.Order;

public class CLModel {

	static enum State {
		MAIN, ORDER, MENU, CUSTOMER, CANCEL_ORDER, ACTIVE_VIEW, QUIT
	};

	private CustomerTracker cTrack;
	private OrderTracker oTrack;
	private ItemTracker iTrack;
	private CLControl control;

	/**
	 * Constructs a CLModel bound to a CLControl with instances customer and
	 * item trackers.
	 * 
	 * @param control
	 *            - The CLControl instance.
	 * @param cTrack
	 *            - Customer data tracking.
	 * @param iTrack
	 *            - Item data tracking.
	 */
	CLModel(CLControl control,OrderTracker oTrack, CustomerTracker cTrack, ItemTracker iTrack) {
		this.oTrack = oTrack;
		this.cTrack = cTrack;
		this.iTrack = iTrack;
		this.control = control;
	}

	/**
	 * Does IO operations needed to run the CLI main menu.
	 * 
	 * @return the next CLI state.
	 */
	public State doMainMenu() {
		String[] opts = { "Orders", "Customer Info", "Menu Options",
				"View System Activity", "Exit" };
		control.writeToView("Please select an option below.");
		control.writeToView(options(opts));
		int selection = control.getInputInt(0, opts.length);
		switch (selection) {
		case 0:
			return State.ORDER;
		case 1:
			return State.CUSTOMER;
		case 2:
			return State.MENU;
		case 3:
			return State.ACTIVE_VIEW;
		case 4:
			return State.QUIT;
		}
		return State.MAIN; // stay in main default
	}

	public void doStartDay() {
		control.writeToView("Welcome to the Pizza Delivery System.\nDo you wish to edit your store configuration?");
		control.writeToView(options(new String[]{"No","Yes"}));
		int selection = control.getInputInt(0, 2);
		switch(selection){
		case 0:
			break;
		case 1:
			doEditCapital();
			break;
		}
		oTrack.startDay();
	}
	
	public void doEditCapital() {
		control.writeToView("Please enter some information about your establishment.");
		control.writeToView("How many food preparation chefs do you employ?:");
		int chefs = control.getInputInt(1, Integer.MAX_VALUE);
		control.writeToView("How many ovens do you own?:");
		int ovens = control.getInputInt(1, Integer.MAX_VALUE);
		control.writeToView("How much space does each oven have?:");
		int ospace = control.getInputInt(1, Integer.MAX_VALUE);
		control.writeToView("How many delivery cars do you have?:");
		int cars = control.getInputInt(1, Integer.MAX_VALUE);
		oTrack.initStations(chefs, ovens, ospace, cars);
	}

	public void doOrderOptions() {
		control.writeToView("Order Options");
		control.writeToView(options(new String[] { "Take Order", "View Orders",
				"Cancel Order" }));
		switch (control.getInputInt(0, 3)) {
		case 0:
			doMakeOrder();
			break;
		case 1:
			doViewOrders();
			break;
		case 2:
			doCancelOrder();
			break;
		}
	}

	/**
	 * Does IO operations need to make an order in the CLI.
	 */
	public void doMakeOrder() {
		List<Customer> customers = getCustomers();
		List<Item> items = getStockedItems();
		List<Destination> pl = getDestinations();
		if (customers.size() == 0) {
			control.writeToView("!Error! You need to have at least one customer first.");
		} else if (items.size() == 0) {
			control.writeToView("!Error! You need to create at least one item first.");
		} else {
			control.writeToView("Create Order:");
			// cusotmer selection
			control.writeToView("Select Customer:");
			control.writeToView(options(customers));
			Customer c = customers
					.get(control.getInputInt(0, customers.size()));
			// item selection
			int input = 0;
			ArrayList<Item> oitems = new ArrayList<Item>();
			do {
				control.writeToView("Select Items (-1 to finish):");
				control.writeToView(options(items));
				input = control.getInputInt(-1, items.size());
				if (input >= 0) {
					oitems.add(items.get(input).clone());
				}
			} while (input >= 0);
			// destination selection
			control.writeToView("Select Destination:");
			control.writeToView(options(pl));
			Destination dest = pl.get(control.getInputInt(0, pl.size()));
			// init order object
			Order order = new Order(oitems, c, dest);
			String prices = "";
			for (Item i : order.getItems()) {
				prices += String.format("\n%1$5.2f - %2$s", i.getPrice(),
						i.getName());
			}
			control.writeToView(prices);
			control.writeToView("---------------");
			control.writeToView(String.format("%1$5s - Total", DecimalFormat
					.getCurrencyInstance().format(order.getPrice())));
			control.writeToView("Confirm order submission");
			control.writeToView(options(new String[] { "Submit", "Cancel" }));
			switch (control.getInputInt(0, 2)) {
			case 0: // submit
				control.writeToView("Submitted: " + order.toString());
				oTrack.addData(order);
				break;
			case 1: // cancel
				control.writeToView("Cancelled Order Submission");
				break;
			}
		}
	}

	/**
	 * Does IO operations needed to access customer options in the CLI.
	 */
	public void doCustomerOptions() {
		control.writeToView("Customer Options");
		control.writeToView(options(new String[] { "New Customer",
				"View Customers" }));
		switch (control.getInputInt(0, 2)) {
		case 0:
			doMakeCustomer();
			break;
		case 1:
			doViewCustomers();
			break;
		}
	}

	/**
	 * Does IO operations needed to view all customers in the CLI.
	 */
	public void doViewCustomers() {
		control.writeToView("Customer List:");
		control.writeToView(options(getCustomers()));
		control.writeToView("Press enter to continue.");
		control.getRawInput();
	}

	/**
	 * Does IO operations needed to make a customer in the CLI.
	 */
	public void doMakeCustomer() {
		control.writeToView("Create Customer");
		control.writeToView("Enter Name:");
		String name = control.getRawInput();
		control.writeToView("Enter Phone #:");
		String pnum = "";
		while (true) {
			pnum = control.getRawInput();
			try {
				Customer cst = new Customer(name, pnum);
				cTrack.addData(cst);
				break;
			} catch (IllegalArgumentException e) {
				control.writeToView("Please enter a valid phone number.");
			}
		}
	}

	/**
	 * Does IO operations needed to edit the menu in the CLI.
	 */
	public void doMenuOptions() {
		control.writeToView("Menu Options");
		control.writeToView(options(new String[] { "New Item", "View Menu",
				"Delete Item" }));
		switch (control.getInputInt(0, 2)) {
		case 0:
			doMakeItem();
			break;
		case 1:
			doViewMenu();
			break;
		case 2:
			doDeleteItem();
			break;
		}
	}

	/**
	 * Does IO operations needed to view the menu in the CLI.
	 */
	private void doViewMenu() {
		control.writeToView("Menu Overview");
		control.writeToView(options(getStockedItems()));
		control.writeToView("Press enter to return.");
		control.getRawInput();
	}

	/**
	 * Does IO operations needed to make a new item in the CLI.
	 */
	private void doMakeItem() {
		control.writeToView("Create Item");
		control.writeToView("Enter Item Name:");
		String iname = control.getRawInput();
		control.writeToView("Enter Prep Time:");
		int prep = control.getInputInt(0, Integer.MAX_VALUE);
		control.writeToView("Enter Cook Time:");
		int cook = control.getInputInt(0, Integer.MAX_VALUE);
		control.writeToView("Enter Oven Space:");
		int space = control.getInputInt(0, Integer.MAX_VALUE);
		control.writeToView("Enter Item Price:");
		double price = control.getInputDouble(0, Double.MAX_VALUE);
		Item item = new Item(iname, price, prep, cook, space);
		iTrack.addData(item);
	}

	/**
	 * Does IO operations needed to delete an existing item in the CLI.
	 */
	private void doDeleteItem() {
		if (getStockedItems().size() == 0) {
			control.writeToView("!Error! There are no items to delete.");
		} else {
			control.writeToView("Operation Unimplemented");
		}
	}

	/**
	 * Does IO operations needed to cancel an order in the CLI.
	 */
	public void doCancelOrder() {
		List<Order> orders = getActiveOrders();
		if (orders.size() > 0) {
			control.writeToView("Cancel Order:");
			control.writeToView("Choose Order to cancel:");
			control.writeToView(options(orders));
			int selection = control.getInputInt(0, orders.size());
			oTrack.cancelOrder(orders.get(selection));
		} else {
			control.writeToView("No orders to cancel.");
		}
	}

	/**
	 * Does IO operations needed to view orders in the CLI.
	 */
	public void doViewOrders() {
		control.writeToView("Active Orders:");
		control.writeToView(options(getActiveOrders()));
		control.writeToView("Press Enter to continue.");
		control.getRawInput();
	}

	/**
	 * Does IO operations needed to view the kitchen's activity in the CLI.
	 */
	public void doActiveView() {
		control.writeToView("Press Enter to return.");
		control.setLogging(true);
		control.getRawInput();
		control.setLogging(false);
	}

	/**
	 * Shuts down the CLModel instance normally and safely. Tries to save customer,
	 * order and item states to files.
	 * 
	 */
	void shutdown() {
		oTrack.endDay();
		try {
			cTrack.saveToFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		try {
			oTrack.saveToFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		try {
			iTrack.saveToFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Returns all items available for ordering.
	 * 
	 * @return all items available for ordering.
	 */
	List<Item> getStockedItems() {
		return iTrack.getData();
	}

	/**
	 * Returns all active orders.
	 * 
	 * @return all active orders.
	 */
	List<Order> getActiveOrders() {
		return oTrack.getData();
	}

	/**
	 * Returns all customers in the database.
	 * 
	 * @return all customers in the database.
	 */
	List<Customer> getCustomers() {
		return cTrack.getData();
	}

	/**
	 * Returns all deliverable destinations.
	 * 
	 * @return all deliverable destinations.
	 */
	List<Destination> getDestinations() {
		return Arrays.asList(Destination.values());
	}

	/**
	 * Enumerates an array of string options with their numerical ordering.
	 * 
	 * @param strings
	 *            - The string options.
	 * @return A string with one option per line.
	 */
	private static String options(Object[] strings) {
		return options(Arrays.asList(strings));
	}

	/**
	 * Enumerates a list of options with their numerical ordering. List elements
	 * should override default toString method for better results.
	 * 
	 * @param strings
	 *            - The string options.
	 * @return A string with one option per line.
	 */
	private static String options(List<?> strings) {
		String str = "";
		for (int i = 0; i < strings.size(); i++) {
			str += String.format("\t(%1d) - %2s\n", i, strings.get(i)
					.toString());
		}
		return str;
	}
}
