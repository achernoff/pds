package cli;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * The view component of the CLI. Handles input and output streams for the
 * command line.
 * 
 * @author acc3863
 * 
 */
class CLView extends Handler {

	private PrintStream output;
	private Scanner scan;
	private boolean enableLogging;

	/**
	 * Instantiates a CLView tied to an input and output stream.
	 * 
	 * @param input
	 *            - The CLView's input stream.
	 * @param output
	 *            - The CLView's output stream.
	 */
	public CLView(CLControl control, InputStream input, PrintStream output) {
		this.output = output;
		this.scan = new Scanner(input);
		enableLogging = false;
	}

	/**
	 * Sets whether the CLView should output activity logs from the kitchen and
	 * delivery stations.
	 * 
	 * @param log
	 *            - True to log, false otherwise.
	 */
	public void setEnableLogging(boolean log) {
		this.enableLogging = log;
	}

	/**
	 * Writes a message to the CLView's output stream.
	 * 
	 * @param message
	 *            - Message to write.
	 */
	public void write(String message) {
		output.println(message);
	}

	/**
	 * Reads a line from the CLView's input stream. Waits if no line is entered.
	 * 
	 * @return String read from input.
	 */
	public String getInputLine() {
		return scan.nextLine();
	}

	/**
	 * No implementation necessary for override.
	 */
	@Override
	public void close() throws SecurityException {
	}

	/**
	 * No implementation necessary for override.
	 */
	@Override
	public void flush() {
	}

	/**
	 * Publishes a LogRecord to the CLView's output stream.
	 * 
	 * @param record
	 *            - the LogRecord to publish
	 */
	@Override
	public void publish(LogRecord record) {
		if (enableLogging) {
			output.println(record.getMessage());
		}
	}
}
