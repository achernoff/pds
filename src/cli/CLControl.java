package cli;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import data.CustomerTracker;
import data.ItemTracker;
import data.OrderTracker;

/**
 * The control component of the CLI MVC system. Controls an instance of CLIView
 * and manages the user's interaction with the PDS system. Intermediates actions
 * between the CLView and CLModel classes.
 * 
 * @author acc3863
 * 
 */
public class CLControl {

	private CLView view;
	private CLModel model;

	/**
	 * Initializes a standard CLControl object.
	 */
	public CLControl() {
		// setup data sources
		CustomerTracker ct = new CustomerTracker();
		ItemTracker it = new ItemTracker();
		OrderTracker ot = new OrderTracker();
		try {
			ct.loadFromFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		try {
			it.loadFromFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		try {
			ot.loadFromFile();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		// setup view
		view = new CLView(this, System.in, System.out);
		// setup model
		model = new CLModel(this, ot, ct, it);
		// setup loggers
		setupLoggers();
	}

	/**
	 * Sets up system loggers for kitchen viewing.
	 */
	private void setupLoggers() {
		Logger master = Logger.getLogger("PDS");
		Logger klog = Logger.getLogger("PDS.Kitchen");
		Logger dlog = Logger.getLogger("PDS.Delivery");

		master.setLevel(Level.FINEST);
		klog.setLevel(Level.FINEST);
		dlog.setLevel(Level.FINEST);

		master.addHandler(view);
	}

	/**
	 * Starts the IO loop that functions as a CLI.
	 * 
	 * @return The OrderTracker used by the CLI.
	 */
	public void startCLI() {
		model.doStartDay();
		boolean quit = false;
		while (!quit) {
			CLModel.State next = model.doMainMenu();
			switch (next) {
			case MAIN:
				continue;
			case CUSTOMER:
				model.doCustomerOptions();
				break;
			case ORDER:
				model.doOrderOptions();
				break;
			case MENU:
				model.doMenuOptions();
				break;
			case ACTIVE_VIEW:
				model.doActiveView();
				break;
			case QUIT:
				if (confirmShutdown())
					quit = true;
				break;
			}
		}
		shutdown();
	}

	/**
	 * Enables or disables the CLView's kitchen and delivery station logging.
	 * 
	 * @param log
	 *            - True to enable, false otherwise.
	 */
	public void setLogging(boolean log) {
		view.setEnableLogging(log);
	}

	/**
	 * Writes a string to the view.
	 * 
	 * @param message
	 *            - String to write.
	 */
	public void writeToView(String message) {
		view.write(message);
	}

	/**
	 * Reads valid integer input from the view.
	 * 
	 * @param min
	 *            - The inclusive minimum acceptable integer.
	 * @param max
	 *            - The non-inclusive maximum acceptable integer.
	 * @return The integer read.
	 */
	public int getInputInt(int min, int max) {
		boolean valid = false;
		int num = 0;
		do {
			try {
				num = Integer.parseInt(getRawInput());
				valid = num >= min && num < max;
				if (!valid)
					throw new NumberFormatException();
			} catch (NumberFormatException nfe) {
				view.write(String.format(
						"Please enter an integer between %1d and %2d", min,
						max - 1));
			}
		} while (!valid);
		return num;
	}

	/**
	 * Reads valid double input from the view.
	 * 
	 * @param min
	 *            - The inclusive minimum acceptable integer.
	 * @param max
	 *            - The non-inclusive maximum acceptable integer.
	 * @return The double read.
	 */
	public double getInputDouble(double min, double max) {
		boolean valid = false;
		double num = 0;
		do {
			try {
				num = Double.valueOf(getRawInput());
				valid = num >= min && num < max;
				if (!valid)
					throw new NumberFormatException();
			} catch (NumberFormatException nfe) {
				view.write(String.format(
						"Please enter an number between %1f and %2e", min, max));
			}
		} while (!valid);
		return num;
	}

	/**
	 * Reads string input from the command line.
	 * 
	 * @return - The string read
	 */
	public String getRawInput() {
		String input = "";
		while (true) {
			input = view.getInputLine();
			if (input.length() > 0) {
				break;
			} else {
				view.write("Please enter something.");
			}
		}
		return input;
	}

	/**
	 * Confirms that the user really intends to quit.
	 */
	public boolean confirmShutdown() {
		view.write("Do you really want to quit? (y/n)");
		return view.getInputLine().trim().charAt(0) == 'y';
	}

	/**
	 * Shuts down the entire CLI and exits the program.
	 */
	private void shutdown() {
		model.shutdown();
		view.close();
	}

	/**
	 * Starts a normal run of the CLI
	 * 
	 * @param args
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable {
		CLControl control = new CLControl();
		control.startCLI();
		System.exit(0); // why do I need to do this...
	}

	/**
	 * Gets an instance of CLControl for use with testing from an input file.
	 * 
	 * @param o
	 *            - the OrderTracker to use
	 * @param i
	 *            - the ItemTracker to use
	 * @param c
	 *            - the CustomerTracker to use
	 * @param file
	 *            - the input file to use
	 * @return the test CLControl
	 * @throws FileNotFoundException
	 *             input file didn't exist
	 */
	public static void runTestControl(OrderTracker o, ItemTracker i,
			CustomerTracker c, java.io.File file) throws FileNotFoundException {
		CLControl control = new CLControl();
		control.model = new CLModel(control, o, c, i);
		java.io.InputStream in = new java.io.FileInputStream(file);
		control.view = new CLView(control, in, System.out);
		control.startCLI();
	}
}
